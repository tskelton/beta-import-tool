﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Configuration;

namespace LargeSQLBatch
{
    public partial class SqlBatch : Form
    {
        public SqlBatch()
        {
            InitializeComponent();
        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void SqlBatch_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog diag = new OpenFileDialog();
            if(diag.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = diag.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            connection.Open();

            FileInfo file = new FileInfo(textBox1.Text);

            button1.Enabled = false;
            button2.Enabled = false;

            using(var reader = new StreamReader(file.OpenRead()))
            {
                var lineCount = File.ReadLines(file.FullName).Count();
                progressBar1.Minimum = 0;
                progressBar1.Maximum = lineCount;
                string line = string.Empty;
                int count = 0;
                label4.Text = file.Name;

                while ((line = reader.ReadLine()) != null)
                {
                    StringBuilder script = new StringBuilder();
                    var values = line.Split(',');

                    script.Append("INSERT INTO ");
                    script.Append(ConfigurationManager.AppSettings["Table"]);
                    script.Append(" VALUES (");
                    script.Append("'" + values[1] + "', ");
                    script.Append("'" + values[0] + "', ");
                    script.Append("'" + values[2] + "');");

                    try
                    {
                        SqlCommand command = new SqlCommand(script.ToString(), connection);
                        command.ExecuteNonQuery();
                        label5.Text = count.ToString();
                        script.Clear();
                    }
                    catch (SqlException sqle)
                    {
                        DialogResult result = MessageBox.Show(sqle.Message + "\r\nWould you like to continue?", "Sql Exception", MessageBoxButtons.YesNo);
                        if (result == DialogResult.No)
                        {
                            break;
                        }
                        script.Clear();
                    }

                    ++count;
                    progressBar1.Value = count;
                    Application.DoEvents();
                }
            }

            connection.Close();
            MessageBox.Show("Process Complete!", "Result", MessageBoxButtons.OK);

            button1.Enabled = true;
            button2.Enabled = true;
        }
    }
}
